'''
Project 2
Traffic problem
Main file
'''
import time
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
from road import *
from car import *
#plt.rcParams['animation.ffmpeg_path'] = '/Users/tianyuwang/Downloads/SnowLeopard_Lion_Mountain_Lion_Mavericks_Yosemite_El-Captain_16.11.2015.zip/ffmpeg'


### Part 1 ####
traffic_counter = 0
cars_entered = 0

def time_step(road, t):
	global traffic_counter
	global cars_entered

	objs = len(road.car_list)

	if np.random.random()< 0.0002*road.num_of_cars:
		print 'Watch out! A deer at t = ', (t - 50) # Deer message. Happens above info for deer loc
		road.insert_deer()
	if objs > 0:
		road.car_list[0].update_pos() # Move the first car
	if objs > 1:
		for i in range(1, objs): # Moves each succesive car
			road.car_list[objs - i].move(road.car_list[objs - i - 1])


	if cars_entered < 1000:
		road.enter(Car(pos = -50))
		cars_entered += 1

		# Old way in comments

		# if traffic_counter > 0:
		# 	traffic_counter -= 1
		# elif len(road.car_list) == 0:
		# 	road.enter(Car())
		# 	cars_entered += 1
		# elif road.car_list[len(road.car_list) - 1].pos < 0.75:
		# 	traffic_counter = 2 # n second delay on entering
		# else:
		# 	road.enter(Car()) # Each time step a new car enters (until 1000 cars entered)
		# 	cars_entered += 1

	road.check_exit() # Check to see if a car exits and remove them if they do

	# Tells us when the last car enters and exits our stretch of the PA turnpike
	if cars_entered == 1000 and road.car_list[len(road.car_list) - 1].pos >= 0:
		print "The last car entered the road at time %d" %(t - 50)
		cars_entered += 1
	if road.car_list == [] and traffic_counter == 0:
		print "The last car left the road at time %d" %(t - 50)
		traffic_counter += 1


PA_turnpike = Road() # Our road


# filter all 0's in a list
# I do not want to draw cars at pos=0 and pos>100
def filter(a_list): 
	return [elem for elem in a_list if elem > 0 and elem <= 100]

# set up plot frame
fig = plt.figure()
ax = plt.axes(xlim=(-3, 105), ylim=(-2, 2))

# object of all car positions
# object of all deer positions
# object of time indicator

dots, = ax.plot([], [], 'bo', ms = 3)
deer_dots, = plt.plot([], [], 'r*', ms = 10)
time_template = 'time = %.1fs'
time_text = ax.text(0.05, 0.9, '', transform = ax.transAxes)

# Set up formatting for the movie files
Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)


# initial frame, draws nothing
def init():
    dots.set_data([], [])
    deer_dots.set_data([],[])
    time_text.set_text('')
    return dots, deer_dots, time_text

# animation part, i iterates from 0 to frames-1
def animate(i):
	x = np.zeros(500) # initiate to register car positions
	x_deers = []
	time_step(PA_turnpike, i)
	for j in range(len(PA_turnpike.car_list)):
		x[j]=PA_turnpike.car_list[j].pos

	x = filter(x)
	dots.set_data(x, np.zeros(len(x)))
	for deer in PA_turnpike.deer_positions:
		if deer[1] > 0:
			x_deers.append(deer[0])
			deer[1] -= 1


	deer_dots.set_data(x_deers,np.zeros(len(x_deers)))

	time_text.set_text(time_template%(i))
	return dots, deer_dots, time_text


anim = animation.FuncAnimation(fig, animate, init_func = init,
                               frames = 1400, interval = 100, blit = True, repeat = False)
anim.save('im.mp4', writer=writer)
plt.show()
