~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
README
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This README will talk about each component of our project, how it works and the
thinking behind our coding.



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Car.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~
Overview
~~~~~~~~
car.py contains the Car class for our project. The car class has three
attributes and two methods as listed below:

			Public					Private
			- move()				- update_pos()
			- speed_lim				- pos - position
									- vel - velocity

	Note: speed_lim while it is intended to be accessed publicly if one
	wanted to change the speed_lim of a particular car, it is suggested
	for uniformity that you change the speed limit of the entire car
	class.


~~~~~~~~~~~
Move Method
~~~~~~~~~~~
Our move method is designed to iterate from the car closest to the entrance of
the road to the car furthest along (nearest the exit). With the way we set up
the speed changes, this iteration will create the appropriate time delay for
decellerating and accelerating.

There are two main logical steps. First, if the car would crash into an object
ahead of it on the road, it will slow down so that it moves 2/3 of the distance
to the location of the object ahead. Here we define 'crash' to be exceeding the
position of the object ahead. Thus if cars are procedeing at a vel of 1 and
are spaced 1 unit apart, they will progress normally.

Second, if a car has space to accelerate, it will add to its current velocity
2/3 of the distance gap between the cars position in the next timestep and the
car_ahead's position in the current timestep.

A couple of test-case checks were also added. If there is no car ahead (ie. the
car_ahead variable is None), then the car will automatically accelerate to the
speed limit. On the other end, if a car would accelerate to a value higher than
the speed limit, it's velocity will only increase to the speed limit.


~~~~~~~~~~~
Speed limit
~~~~~~~~~~~
Different cars have different speed capabilities and not all drivers listen to
the speed limit. This attribute allows for this to be tampered with but for
purposes of our project it will remain constant and the same for every car


~~~~
Test
~~~~
If you run the car.py file you will see a readout of positions of 4 cars (a, b,
c and d) over a time period of 20 time-steps. Here you will notice a decrease
in velocity when a collision is about to occur and after slowing and returning
to the speed limit, a larger gap between each car than was started with. This
matches our expectations for our codes function




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Road.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~
Overview
~~~~~~~~
road.py houses our Road class. This object stores a list of objects in order of
position (cars and deer) contained in the 100 unit stretch of road we observe.
Below are the methods and attributes of this class with their intended uses.


			Public					Private
			- enter()				- exit()
			- check_exit()			- num_deer
			- insert_deer()			- i - index for deer loc
			- num_of_cars
			- car_list

	Note: num_of_cars and car_list are listed aspublic not because they
	should be changed publicly but because the data contained is meant
	to be accesed publicly (eg. accesing positions of cars from car_list)


~~~~~~~~~~~~~~~~~
Enter/Exit Method
~~~~~~~~~~~~~~~~~
The enter and exit methods add and remove cars from the road (aka. the list of
cars). Since each newly added car will be entering the stretch of road at
position zero, it will be the newest car and thus is just appended to the list
of cars. Since we could have multiple cars exit in one time step and also have
a deer exit as well, the exit method removes an element by appending the lists
on either side of the element of choice.


~~~~~~~~~~~~~~~~~
Check_Exit Method
~~~~~~~~~~~~~~~~~
The check_exit method is the publically called method that chooses whether an
object on the road has left it and needs to be removed from the car_list. First
it checks whether there is a deer and removes it from the list. Next it will
check if there are any cars in the list. If there are, it will check to see if
the car's position is greater than the stretch of road observed. If it is it
will call the exit method, removing that car from the list and call check_exit
again until no cars with a position greater than 100 remain.


~~~~~~~~~~~~~~~~~~
Insert_Deer Method
~~~~~~~~~~~~~~~~~~


NOTE: Currently the way the deer portion of the program is written does not
count the deer as a 'car' that would be on the road. Thus the car counter does
not change when a deer is added or subtracted. (I've adjusted coding in the
road class to account for this)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Main.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contributors:

Chris Nagale
Tianyu Wang
Harrison Schell
Andrew Glaser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
